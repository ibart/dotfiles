return {
  "nvim-tree/nvim-tree.lua",
  config = function()
    vim.keymap.set("n", "<leader>e", "<cmd>NvimTreeToggle<CR>", { desc = "Toggle [E]xplorer" })
    require("nvim-tree").setup({
      hijack_netrw = true,
      auto_reload_on_write = true,
      sort = {
        sorter = "name",
        folders_first = true,
        files_first = false,
      },
      view = {
        width = 50,
      },
      renderer = {
        group_empty = true,
        root_folder_label = ":t",
      },
      hijack_directories = {
        enable = false,
        auto_open = true,
      },
      update_focused_file = {
        enable = true,
        update_root = {
          enable = true,
          ignore_list = {},
        },
        exclude = false,
      },
    })
  end,
}
