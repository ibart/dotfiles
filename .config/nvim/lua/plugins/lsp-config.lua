return {
  {
    "williamboman/mason.nvim",
    config = function()
      -- setup mason with default properties
      require("mason").setup()
    end,
  },
  -- mason lsp config utilizes mason to automatically ensure lsp servers you want installed are installed
  {
    "williamboman/mason-lspconfig.nvim",
    config = function()
      -- ensure that we have lua language server, typescript launguage server, java language server, and java test language server are installed
      require("mason-lspconfig").setup({
        ensure_installed = { "lua_ls", "ts_ls", "jdtls" },
      })
    end,
  },
  -- mason nvim dap utilizes mason to automatically ensure debug adapters you want installed are installed, mason-lspconfig will not automatically install debug adapters for us
  {
    "jay-babu/mason-nvim-dap.nvim",
    config = function()
      -- ensure the java debug adapter is installed
      require("mason-nvim-dap").setup({
        ensure_installed = { "java-debug-adapter", "java-test" },
      })
    end,
  },
  -- utility plugin for configuring the java language server for us
  {
    "mfussenegger/nvim-jdtls",
    dependencies = {
      "mfussenegger/nvim-dap",
    },
  },
  {
    "neovim/nvim-lspconfig",
    config = function()
      vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = "rounded" })
      vim.lsp.handlers["textDocument/signatureHelp"] =
        vim.lsp.with(vim.lsp.handlers.signature_help, { border = "rounded" })
      require("lspconfig.ui.windows").default_options.border = "rounded"

      -- get access to the lspconfig plugins functions
      local lspconfig = require("lspconfig")

      local capabilities = require("cmp_nvim_lsp").default_capabilities()

      -- setup the lua language server
      lspconfig.lua_ls.setup({
        capabilities = capabilities,
      })

      -- setup the angular language server
      local angularls_path = require("mason-registry").get_package("angular-language-server"):get_install_path()
      local angularls_cmd = {
        "ngserver",
        "--stdio",
        "--tsProbeLocations",
        angularls_path .. "/node_modules/typescript",
        "--ngProbeLocations",
        angularls_path .. "/node_modules/@angular/language-server",
      }
      lspconfig.angularls.setup({
        capabilities = capabilities,
        cmd = angularls_cmd,
        on_new_config = function(new_config, new_root_dir)
          new_config.cmd = angularls_cmd
        end,
      })

      -- setup the typescript language server
      lspconfig.ts_ls.setup({
        capabilities = capabilities,
      })

      -- setup the css language server
      lspconfig.cssls.setup({
        capabilities = capabilities,
      })

      -- setup the html language server
      lspconfig.html.setup({
        capabilities = capabilities,
      })

      -- setup the bash language server
      lspconfig.bashls.setup({
        capabilities = capabilities,
      })

      -- setup the csharp language server
      -- lspconfig.csharp_ls.setup({
      --   capabilities = capabilities,
      -- })

      -- setup the python language server
      lspconfig.pyright.setup({
        capabilities = capabilities,
      })

      local pid = vim.fn.getpid()
      local omnisharp_path = require("mason-registry").get_package("omnisharp"):get_install_path()
        .. "/libexec/OmniSharp.dll"
      lspconfig.omnisharp.setup({
        capabilities = capabilities,
        cmd = { "dotnet", omnisharp_path, "--languageserver", "--hostPID", tostring(pid) },
        settings = {
          FormattingOptions = {
            EnableEditorConfigSupport = false,
            NewLinesForBracesInTypes = false,
            NewLinesForBracesInMethods = false,
            NewLinesForBracesInProperties = false,
            NewLinesForBracesInAccessors = false,
            NewLinesForBracesInAnonymousMethods = false,
            NewLinesForBracesInControlBlocks = false,
            NewLinesForBracesInAnonymousTypes = false,
            NewLinesForBracesInObjectCollectionArrayInitializers = false,
            NewLinesForBracesInLambdaExpressionBody = false,
            NewLineForElse = false,
            NewLineForCatch = false,
            NewLineForFinally = false,
            NewLineForMembersInObjectInit = false,
            NewLineForMembersInAnonymousTypes = false,
            NewLineForClausesInQuery = false,
          },
        },
      })

      -- Set vim motion for <Space> + c + h to show code documentation about the code the cursor is currently over if available
      vim.keymap.set("n", "<leader>ch", vim.lsp.buf.hover, { desc = "[C]ode [H]over Documentation" })
      -- Set vim motion for <Space> + c + d to go where the code/variable under the cursor was defined
      vim.keymap.set("n", "<leader>cd", vim.lsp.buf.definition, { desc = "[C]ode Goto [D]efinition" })
      -- Set vim motion for <Space> + c + a for display code action suggestions for code diagnostics in both normal and visual mode
      vim.keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, { desc = "[C]ode [A]ctions" })
      -- Set vim motion for <Space> + c + r to display references to the code under the cursor
      vim.keymap.set(
        "n",
        "<leader>cr",
        require("telescope.builtin").lsp_references,
        { desc = "[C]ode Goto [R]eferences" }
      )
      -- Set vim motion for <Space> + c + i to display implementations to the code under the cursor
      vim.keymap.set(
        "n",
        "<leader>ci",
        require("telescope.builtin").lsp_implementations,
        { desc = "[C]ode Goto [I]mplementations" }
      )
      -- Set a vim motion for <Space> + c + <Shift>R to smartly rename the code under the cursor
      vim.keymap.set("n", "<leader>cR", vim.lsp.buf.rename, { desc = "[C]ode [R]ename" })
      -- Set a vim motion for <Space> + c + <Shift>D to go to where the code/object was declared in the project (class file)
      vim.keymap.set("n", "<leader>cD", vim.lsp.buf.declaration, { desc = "[C]ode Goto [D]eclaration" })
    end,
  },
}
