return {
  "nvimtools/none-ls.nvim",
  dependencies = {
    "nvimtools/none-ls-extras.nvim",
  },
  config = function()
    -- get access to the none-ls functions
    local null_ls = require("null-ls")
    -- run the setup function for none-ls to setup our different formatters
    null_ls.setup({
      sources = {
        null_ls.builtins.formatting.stylua.with({
          extra_args = {
            "--indent-type=Spaces",
            "--indent-width=2",
          },
        }),
        null_ls.builtins.formatting.prettier,
        null_ls.builtins.formatting.black,
        null_ls.builtins.formatting.shfmt.with({
          extra_args = {
            "--indent=4",
            "--case-indent",
          },
        }),
      },
    })

    -- set up a vim motion for <Space> + c + f to automatically format our code based on which langauge server is active
    vim.keymap.set(
      "n",
      "<leader>cf",
      "<cmd>lua vim.lsp.buf.format({ timeout_ms = 5000 })<CR>",
      { desc = "[C]ode [F]ormat" }
    )
  end,
}
