return {
  "folke/which-key.nvim",
  event = "VimEnter",
  config = function()
    -- gain access to the which key plugin
    local which_key = require("which-key")

    -- call the setup function with default properties
    which_key.setup({
      delay = 500,
      -- Register prefixes for the different key mappings we have setup previously
      spec = {
        { "<leader>/", group = "Comments" },
        { "<leader>b", group = "[B]uffer" },
        { "<leader>c", group = "[C]ode" },
        { "<leader>d", group = "[D]ebug" },
        { "<leader>e", group = "[E]xplorer" },
        { "<leader>f", group = "[F]ind" },
        { "<leader>g", group = "[G]it" },
        { "<leader>J", group = "[J]ava" },
        { "<leader>r", group = "[R]un" },
      },
    })
  end,
}
