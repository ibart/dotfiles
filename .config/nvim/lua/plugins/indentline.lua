return {
  "lukas-reineke/indent-blankline.nvim",
  config = function()
    require("ibl").setup({
      indent = {
        char = "│",
        smart_indent_cap = true,
      },
      scope = {
        show_start = false,
      },
    })
  end,
}
