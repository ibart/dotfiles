local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }

local function desc(text)
  return { noremap = true, silent = true, desc = text }
end

keymap("n", "<Space>", "", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

keymap("n", "<leader>q", "<cmd>confirm q<CR>", desc("Quit"))
keymap("n", "<leader>w", "<cmd>w!<CR>", desc("Save"))
keymap("n", "<Esc>", "<cmd>nohlsearch<CR>", desc("Remove search highlights"))
keymap("t", "<Esc><Esc>", "<C-\\><C-n>", desc("Exit terminal mode"))

keymap("n", "<leader>rw", "<cmd>wa<CR><cmd>!dotnet build --no-restore && godot-mono<CR>", desc("Build and run godot"))
keymap("n", "<leader>rg", "<cmd>wa<CR><cmd>!godot-mono<CR>", desc("Run godot"))
keymap("n", "<leader>rc", "<cmd>wa<CR><cmd>!dotnet build --no-restore<CR>", desc("Dotnet build"))
keymap("n", "<leader>rl", "<cmd>wa<CR><cmd>!pdflatex '%:t'<CR>", desc("LateX"))
keymap("n", "<leader>rp", "<cmd>wa<CR><cmd>!python '%:t'<CR>", desc("Python"))
keymap("n", "<leader>rs", "<cmd>wa<CR><cmd>silent !wtype -M logo -k j && wtype -M ctrl -k r && wtype -M logo -k k && wtype -m logo<CR>", desc("Server"))

-- Better window navigation
keymap("n", "<m-h>", "<C-w>h", opts)
keymap("n", "<m-j>", "<C-w>j", opts)
keymap("n", "<m-k>", "<C-w>k", opts)
keymap("n", "<m-l>", "<C-w>l", opts)
keymap("n", "<m-tab>", "<c-6>", opts)

keymap("n", "n", "nzz", opts)
keymap("n", "N", "Nzz", opts)
keymap("n", "*", "*zz", opts)
keymap("n", "#", "#zz", opts)
keymap("n", "g*", "g*zz", opts)
keymap("n", "g#", "g#zz", opts)

-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

keymap("x", "p", [["_dP]])

vim.cmd([[:amenu 10.100 mousemenu.Goto\ Definition <cmd>lua vim.lsp.buf.definition()<CR>]])
vim.cmd([[:amenu 10.110 mousemenu.References <cmd>lua vim.lsp.buf.references()<CR>]])
-- vim.cmd [[:amenu 10.120 mousemenu.-sep- *]]

keymap("n", "<RightMouse>", "<cmd>:popup mousemenu<CR>")

-- tailwind bearable to work with
keymap({ "n", "x" }, "j", "gj", opts)
keymap({ "n", "x" }, "k", "gk", opts)
