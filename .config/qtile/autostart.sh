#!/usr/bin/env bash 

xrandr --dpi 96 &
setbg &
picom &
unclutter &
xset r rate 300 50 &
xset dpms 0 0 0 && xset s noblank  && xset s off &
nm-applet &
