import os
import subprocess
from libqtile import bar, hook, layout, qtile, widget #, extension
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy

# Make sure 'qtile-extras' is installed or this config will not work.
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration
#from qtile_extras.widget import StatusNotifier

#from libqtile.log_utils import logger


mod = "mod4"              # Sets mod key to SUPER/WINDOWS
myTerm = "foot"      # My terminal of choice
myBrowser = "librewolf" # My browser of choice

# A list of available commands that can be bound to keys can be found
# at https://docs.qtile.org/en/latest/manual/config/lazy.html
keys = [
    # The essentials
    Key([mod], "Return", lazy.spawn(myTerm), desc="Terminal"),
    Key([mod, "shift"], "Return", lazy.spawn("dmenu_run -i -p Run: "), desc='Run Launcher'),
    Key([mod], "b", lazy.spawn(myBrowser), desc='LibreWolf'),
    Key([mod], "c", lazy.spawn("qalculate-gtk"), desc='Qalculate'),
    Key([mod], "w", lazy.spawn("setbg /repos/wallpapers"), desc='Change Wallpaper'),
    Key([mod, "shift"], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "Tab", lazy.screen.toggle_group(), desc="Toggle between last 2 groups"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "r", lazy.reload_config(), desc="Reload the config"),
    Key(["control"], "escape", lazy.spawn("dm-logout"), desc="Logout menu"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    
    # Switch between windows
    # Some layouts like 'monadtall' only need to use j/k to move
    # through the stack, but other layouts like 'columns' will
    # require all four directions h/j/k/l to move around.
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    #Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),


    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),


    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "space", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),


    # Grow/shrink windows left/right. 
    # This is mainly for the 'monadtall' and 'monadwide' layouts
    # although it does also work in the 'bsp' and 'columns' layouts.
    Key([mod], "equal",
        lazy.layout.grow_left().when(layout=["bsp", "columns"]),
        lazy.layout.grow().when(layout=["monadtall", "monadwide"]),
        desc="Grow window to the left"
    ),
    Key([mod], "minus",
        lazy.layout.grow_right().when(layout=["bsp", "columns"]),
        lazy.layout.shrink().when(layout=["monadtall", "monadwide"]),
        desc="Grow window to the left"
    ),

    # Grow windows up, down, left, right.  Only works in certain layouts.
    # Works in 'bsp' and 'columns' layout.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod], "m", lazy.layout.maximize(), desc='Toggle between min and max sizes'),
    Key([mod], "f", lazy.window.toggle_floating(), desc='toggle floating'),
    Key([mod], "space", lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),

    # Switch focus of monitors
    Key([mod], "period", lazy.screen.next_group(skip_empty = True), desc='Move focus to next group while skipping empty'),
    Key([mod], "comma", lazy.screen.prev_group(skip_empty = True), desc='Move focus to next group while skipping empty'),
    # Key([mod], "period", lazy.next_screen(), desc='Move focus to next monitor'),
    # Key([mod], "comma", lazy.prev_screen(), desc='Move focus to prev monitor'),

    
    # An example of using the extension 'CommandSet' to give 
    # a list of commands that can be executed in dmenu style.
    # Key([mod], 'z', lazy.run_extension(extension.CommandSet(
    #     commands={
    #         'play/pause': '[ $(mocp -i | wc -l) -lt 2 ] && mocp -p || mocp -G',
    #         'next': 'mocp -f',
    #         'previous': 'mocp -r',
    #         'quit': 'mocp -x',
    #         'open': 'urxvt -e mocp',
    #         'shuffle': 'mocp -t shuffle',
    #         'repeat': 'mocp -t repeat',
    #         },
    #     pre_commands=['[ $(mocp -i | wc -l) -lt 1 ] && mocp -S'],
    #     ))),
    
    KeyChord([mod], "a", [
        Key([], "b", lazy.spawn("dm-bookmark"), desc='Bookmark Script'),
        Key(["shift"], "b", lazy.spawn("dm-bookmark add"), desc='Add a bookmark'),
        Key([], "g", lazy.spawn("gimp"), desc='Gimp'),
        Key([], "i", lazy.spawn("inkscape"), desc='Inkscape'),
        Key([], "k", lazy.spawn("keepassxc"), desc='Keepassxc'),
        Key([], "l", lazy.spawn("localc"), desc='Libreoffice calc'),
        Key([], "o", lazy.spawn("olive-editor-0.2"), desc='Olive Editor'),
        Key([], "t", lazy.spawn("thunderbird"), desc='Thunderbird'),
    ]),

    # Multimedia Keys
    Key([], "XF86AudioMute", lazy.spawn("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"), desc='Toggle volume.'),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("wpctl set-volume @DEFAULT_AUDIO_SINK@ 4%+"), desc='Raise volume.'),
    Key([], "XF86AudioLowerVolume", lazy.spawn("wpctl set-volume @DEFAULT_AUDIO_SINK@ 4%-"), desc='Lower volume.'),
    Key([], "XF86MonBrightnessUp", lazy.spawn("sb-backlight inc"), desc='Raise brightness'),
    Key([], "XF86MonBrightnessDown", lazy.spawn("sb-backlight dec"), desc='Lower brightness'),
    Key([], "XF86Mail", lazy.spawn("dm-bookmark"), desc='Bookmark Script'),
    Key([], "XF86HomePage", lazy.spawn(myBrowser), desc='LibreWolf'),
    Key([], "XF86Launch4", lazy.spawn(myBrowser), desc='LibreWolf'),
    Key([], "Print", lazy.spawn("dm-maim"), desc='Screenshot'),

]
groups = []
#group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
group_name_match = {1:[],
                    2:[Match(wm_class=["LibreWolf"])],
                    3:[],
                    4:[Match(wm_class=["thunderbird"])],
                    5:[],
                    6:[],
                    7:[],
                    8:[Match(wm_class=["Jellyfin Media Player"])],
                    9:[Match(wm_class=["Gimp"]),
                       Match(wm_class=["Inkscape"]),
                       Match(wm_class=["kdenlive"])],
                    }

#group_labels = ["DEV", "WWW", "SYS", "DOC", "VBOX", "CHAT", "MUS", "VID", "GFX",]
group_labels = ["1", "2", "3", "4", "5", "6", "7", "8", "9",]
#group_labels = ["", "", "", "", "", "", "", "", "",]


group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall"]

for i in group_name_match:
    groups.append(
        Group(
            name=f"{i}",
            layout=group_layouts[i - 1].lower(),
            label=group_labels[i - 1],
            matches=group_name_match[i]
        ))
 
for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(i.name),
            ),
        ]
    )


#tokyo_night_storm
colors = [
    '#24283b', # bg
    '#c0caf5', # fg
    '#f7768e', # color01
    '#9ece6a', # color02
    '#e0af68', # color03
    '#7aa2f7', # color04
    '#bb9af7', # color05
    '#7dcfff', # color06
    '#c0caf5', # color15
    ]

# Some settings that I use on almost every layout, which saves us
# from having to type these out for each individual layout.
layout_theme = {"border_width": 2,
                #"single_border_width": 0,
                "margin": 10,
                "border_focus": colors[4],
                "border_normal": colors[0]
                }

layouts = [
    #layout.Bsp(**layout_theme),
    #layout.Floating(**layout_theme)
    #layout.RatioTile(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    layout.MonadTall(**layout_theme),
    #layout.MonadWide(**layout_theme),
    layout.Max(
         border_width = 0,
         margin = 0,
         ),
    #layout.Stack(**layout_theme, num_stacks=2),
    #layout.Columns(**layout_theme),
    # layout.Zoomy(**layout_theme),
]

# Some settings that I use on almost every widget, which saves us
# from having to type these out for each individual widget.
myFont = "JetBrainsMono Nerd Font extrabold"
widget_defaults = dict(
    #font="Ubuntu Bold",
    font = myFont,
    fontsize = 13,
    padding = 0,
    background=colors[0]
)

extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
        # widget.CurrentLayoutIcon(
        #         #custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
        #         foreground = colors[1],
        #         padding = 0,
        #         scale = 0.7
        #         ),
        # widget.CurrentLayout(
        #         foreground = colors[1],
        #         padding = 4
        #         ),
        # widget.Prompt(
        #         font = myFont,
        #         fontsize=14,
        #         foreground = colors[1]
        # ),
        widget.GroupBox(
                #fontsize = 13,
                margin_y = 3,
                margin_x = 4,
                padding_y = 2,
                padding_x = 3,
                borderwidth = 3,
                hide_unused = True,
                active = colors[8],
                inactive = colors[1],
                rounded = False,
                highlight_color = '#555',
                highlight_method = "line",
                this_current_screen_border = colors[4],
                this_screen_border = colors [7],
                other_current_screen_border = colors[4],
                other_screen_border = colors[7],
                ),
        widget.TextBox(
                text = ' ',
                font = myFont,
                foreground = colors[1],
                padding = 2,
                fontsize = 14
                ),
        widget.WindowName(
                foreground = colors[6],
                max_chars = 60
                ),
        # widget.GenPollText(
        #         update_interval = 300,
        #         func = lambda: subprocess.check_output("printf $(uname -r)", shell=True, text=True),
        #         foreground = colors[3],
        #         fmt = '❤  {}',
        #         decorations=[
        #             BorderDecoration(
        #                 colour = colors[3],
        #                 border_width = [0, 0, 2, 0],
        #                 )
        #             ],
        #         ),
        widget.Spacer(length = 8),
        widget.Net(
                format = '\u2193 {down}',
                interfaces = ["enp0s18f2u2", "wlo1"],
                update_interval = 2,
                foreground = colors[6],
                decorations=[
                    BorderDecoration(
                        colour = colors[6],
                        border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
        widget.Spacer(length = 8),
        widget.CPU(
                format = '󰍛 {load_percent:.0f}%',
                foreground = colors[4],
                decorations=[
                    BorderDecoration(
                        colour = colors[4],
                        border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
        widget.ThermalSensor(
                format = ' ({temp:.0f}{unit})',
                foreground = colors[4],
                decorations=[
                    BorderDecoration(
                        colour = colors[4],
                        border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
        widget.Spacer(length = 8),
        widget.Memory(
                foreground = colors[8],
                mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                format = '{MemUsed: .0f}{mm}',
                fmt = '{}',
                decorations=[
                    BorderDecoration(
                        colour = colors[8],
                        border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
        widget.Spacer(length = 8),
        widget.DF(
                update_interval = 60,
                foreground = colors[5],
                mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e df')},
                partition = '/',
                #format = '[{p}] {uf}{m} ({r:.0f}%)',
                format = '{uf}{m}',
                fmt = ' {}',
                visible_on_warn = False,
                decorations=[
                    BorderDecoration(
                        colour = colors[5],
                        border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
        widget.Spacer(length = 8),
        widget.Volume(
                foreground = colors[7],
                fmt = '🕫 {}',
                decorations=[
                    BorderDecoration(
                        colour = colors[7],
                        border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
        widget.Spacer(length = 8),
        widget.KeyboardLayout(
                foreground = colors[8],
                fmt = ' ⌨ {} ',
                configured_keyboards=["us", "ara"],
                mouse_callback={'Button1': lambda : lazy.widget["keyboardlayout"].next_keyboard()},
                decorations=[
                    BorderDecoration(
                        colour = colors[8],
                        border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
        widget.Spacer(length = 8),
        widget.Clock(
                foreground = colors[6],
                format = "󰥔 %H:%M (%a, %d %b)",
                decorations=[
                    BorderDecoration(
                        colour = colors[6],
                        border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
        widget.Spacer(length = 8),
        widget.TextBox(
                text = ' ',
                font = myFont,
                mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('dm-logout')},
                foreground = colors[3],
                decorations=[
                    BorderDecoration(
                        colour = colors[3],
                        border_width = [0, 0, 2, 0],
                        )
                    ],
                padding = 4,
                fontsize = 14
                ),
        widget.Spacer(length = 8),
        widget.Systray(padding = 3),
        widget.Spacer(length = 8),
        ]
    return widgets_list

# I use 3 monitors which means that I need 3 bars, but some widgets (such as the systray)
# can only have one instance, otherwise it will crash.  So I define the follow two lists.
# The first one creates a bar with every widget EXCEPT index 15 and 16 (systray and spacer).
# The second one creates a bar with all widgets.
 
# def init_widgets_screen1():
#     widgets_screen1 = init_widgets_list()
#     del widgets_screen1[22:23]   # Removes widgets 15 and 16 for bars on Monitors 1 + 3
#     return widgets_screen1
#
# def init_widgets_screen2():
#     widgets_screen2 = init_widgets_list()
#     return widgets_screen2       # Monitor 2 will display ALL widgets in widgets_list

# For adding transparency to your bar, add (background="#00000000") to the "Screen" line(s)
# For ex: Screen(top=bar.Bar(widgets=init_widgets_screen2(), background="#00000000", size=24)),

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_list(), size=26)),
            # Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26)),
            # Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=26))
            ]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    # widgets_screen1 = init_widgets_screen1()
    # widgets_screen2 = init_widgets_screen2()

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    border_focus=colors[4],
    border_width=2,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),   # gitk
        Match(wm_class="makebranch"),     # gitk
        Match(wm_class="maketag"),        # gitk
        Match(wm_class="ssh-askpass"),    # ssh-askpass
        Match(title="branchdialog"),      # gitk
        Match(title='Confirmation'),      # tastyworks exit box
        Match(title='Qalculate!'),        # qalculate-gtk
        Match(wm_class='keepassxc'),      # keepassxc
        Match(wm_class='pinentry-gtk-2'), # GPG key password entry
        Match(title="pinentry"),          # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
#wl_input_rules = None


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
