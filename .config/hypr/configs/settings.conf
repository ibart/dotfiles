ecosystem {
    no_update_news = true
}

master {
  new_on_top = 1
  new_status = master
  mfact = 0.5
  allow_small_split = true
}

general {
  gaps_in = 2
  gaps_out = 4
  border_size = 1
  resize_on_border = true

  col.active_border = rgba(ccccffaa)
  col.inactive_border = rgba(222222aa)

  layout = master
}

decoration {
  rounding = 0
  shadow {
      enabled = false
  }

  blur {
    enabled = false
  }
}

animations {
  enabled = true

  bezier = wind, 0.05, 0.9, 0.1, 1.05
  bezier = winOut, 0.3, -0.3, 0, 1
  bezier = slow, 0, 0.85, 0.3, 1
  bezier = overshot, 0.7, 0.6, 0.1, 1.1
  bezier = bounce, 1.1, 1.6, 0.1, 0.85

  animation = windowsIn, 1, 5, slow, popin
  # animation = windowsOut, 1, 5, winOut, slide
  animation = windowsMove, 1, 5, wind, slide
  animation = fade, 1, 5, overshot
  animation = workspaces, 1, 5, wind
  animation = windows, 1, 5, bounce, popin
}

input {
  kb_layout = us,ara
  kb_variant =
  kb_model =
  kb_options = grp:menu_toggle
  kb_rules =
  repeat_rate=50
  repeat_delay=300
  numlock_by_default=0
  left_handed=0
  follow_mouse=1
  float_switch_override_focus=0
  sensitivity = 0

  touchpad {
    disable_while_typing = 0
    natural_scroll = 0
    scroll_factor = 0.2
    clickfinger_behavior = 0
    middle_button_emulation=0
    tap-to-click=1
    drag_lock=0
    tap_button_map = lmr
  }
}

cursor {
  inactive_timeout = 5
  no_break_fs_vrr = true
}

gestures {
  workspace_swipe=1
  workspace_swipe_fingers=3
  workspace_swipe_distance=400
  workspace_swipe_invert=1
  workspace_swipe_min_speed_to_force=30
  workspace_swipe_cancel_ratio=0.5
  workspace_swipe_create_new=1 
  workspace_swipe_forever=1
}

misc {
  disable_hyprland_logo = true
  disable_splash_rendering = true
  mouse_move_enables_dpms = true
  vrr = 2
  vfr = true
  enable_swallow = false
  focus_on_activate = false
  # swallow_regex = ^(foot)$
}

binds {
  workspace_back_and_forth=1
  allow_workspace_cycles=1
  pass_mouse_when_bound=0
}
