#!/usr/bin/env bash

#> config settings
search_prompt="${search_prompt-Search Youtube: }"
#used when getting the html from youtube
useragent=${useragent-'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.152 Safari/537.36'}

#filter id used when searching
sp="${sp-}"
#is used to know whether or not scraping the search page is necessary
scrape="${scrape-yt_search}"

############################
#        Formatting        #
############################
#> Colors  (printf)
c_red="\033[1;31m"
c_green="\033[1;32m"
c_yellow="\033[1;33m"
c_blue="\033[1;34m"
c_magenta="\033[1;35m"
c_cyan="\033[1;36m"
c_reset="\033[0m"

format_fzf() {
    dur_len=7
    view_len=10
    date_len=14
    url_len=12

    frac=10
    title_len=$((frac * 7 + 4))
    channel_len=$((frac * 2))
    dur_len=$((frac * 1))
    view_len=$((frac * 1 + 2))
    date_len=$((frac * 2 - 5))
}
#> Formats the fields depending on which menu is needed. And assigns the menu command.
format_menu() {
    menu_command='fzf -m --bind change:top --tabstop=1 --layout=reverse --delimiter="$(printf "\t")" --nth=1,2 $FZF_DEFAULT_OPTS'
    format_fzf
}

format_video_data() {
    printf "%s" "$*" | awk -F"$(printf '\t')" \
        -v A=$title_len -v B=$channel_len -v C=$dur_len -v D=$view_len -v E=$date_len -v F=$url_len \
        '{ printf "%-"A"."A"s\t%-"B"."B"s\t%-"C"."C"s\t%-"D"."D"s\t%-"E"."E"s\t%-"F"."F"s\n",$1,$2,$4,$3,$5,$6}'

}

############################
#   Video selection Menu   #
############################
video_menu() {
    #take input format it to the appropriate format, then pipe it into the menu
    format_video_data "$*" | eval "$menu_command"
}

############################
#         Scraping         #
############################

download_thumbnails() {
    #scrapes the urls of the thumbnails of the videos from the adjusted json
    thumb_urls="$(printf "%s" "$*" |
        jq -r '.[]|(.thumbs,.videoID)')"
    i=0
    while read -r line; do
        if [ $((i % 2)) -eq 0 ]; then
            url="$line"
            sleep 0.001
        else
            name="$line"
            {
                curl -s "$url" -G --data-urlencode "sqp=" >"$thumb_dir/$name.png"
            } &
        fi
        i=$((i + 1))
    done <<EOF
$thumb_urls
EOF

}
get_sp_filter() {

    #filter_id is a variable that keeps changing throught this function
    filter_id=""

    #sp is the final filter id that is used in the search query
    sp=""

    #the way youtube uses these has a pattern, for example
    #in the sort_by_filter the only difference is the 3rd character, I just don't know how to use this information efficiently
    case "$sort_by_filter" in
        upload-date) filter_id="CAISBAgAEAE" ;;
        view-count) filter_id="CAMSBAgAEAE" ;;
        rating) filter_id="CAESBAgAEAE" ;;
    esac

    #another example is sort by filter + upload date filter only changes one character as well
    if [ -n "$filter_id" ]; then
        #gets the character in the filter_id that needs to be replaced if upload_date_filter is also given
        upload_date_character="$(echo "$filter_id" | awk '{print substr($1, 8, 1)}')"
    fi

    #For each of these, if upload_date_character is unset, the filter_id should be the normal filter
    #Otherwise set the upload_date_character to the right upload_date_character
    case "$upload_date_filter" in
        last-hour) [ -z "$upload_date_character" ] && filter_id="EgQIARAB" || upload_date_character="B" ;;
        today) [ -z "$upload_date_character" ] && filter_id="EgQIAhAB" || upload_date_character="C" ;;
        this-week) [ -z "$upload_date_character" ] && filter_id="EgQIAxAB" || upload_date_character="D" ;;
        this-month) [ -z "$upload_date_character" ] && filter_id="EgQIBBAB" || upload_date_character="E" ;;
        this-year) [ -z "$upload_date_character" ] && filter_id="EgQIBRAB" || upload_date_character="F" ;;
    esac

    #if upload_date_character isn't empty, set sp to upload_date filter + sort_by filter
    if [ -n "$upload_date_character" ]; then
        #replaces the 8th character in the filter_id with the appropriate character
        #the 8th character specifies the upload_date_filter
        sp="$(echo "$filter_id" | sed "s/\\(.\\{7\\}\\)./\\1$upload_date_character/")"
    #otherwise set it to the filter_id
    else
        sp="$filter_id"
    fi
}

get_yt_json() {
    #youtube has a bunch of data relating to videos in a json format, this scrapes that
    printf "%s" "$*" | sed -n '/var *ytInitialData/,$p' | tr -d '\n' |
        sed -E ' s_^.*var ytInitialData ?=__ ; s_;</script>.*__ ;'
}

get_yt_html() {
    link="$1"
    query="$2"
    printf "%s" "$(
        curl "$link" -s \
            -G --data-urlencode "search_query=$query" \
            -G --data-urlencode "sp=$sp" \
            -H 'authority: www.youtube.com' \
            -H "user-agent: $useragent" \
            -H 'accept-language: en-US,en;q=0.9' \
            --compressed
    )"
}

get_video_data() {
    #in that list this finds the title, channel, view count, video length, video upload date, and the video id/url
    printf "%s" "$*" |
        jq -r '.[]| (
			.title,
			.channel,
			.views,
			.duration,
			.date,
			.videoID
			)' | sed "N;N;N;N;N;s/\n/""$(printf '\t')""\|/g"
}

scrape_channel() {
    # needs channel url as $*
    ## Scrape data and store video information in videos_data ( and thumbnails )
    yt_html="$(get_yt_html "$*")"

    if [ -z "$yt_html" ]; then
        print_error "\033[31mERROR[#01]: Couldn't curl website. Please check your network and try again.\033[0m\n"
        exit 1
    fi

    #gets the channel name from title of page
    channel_name="$(
        echo "$yt_html" | grep -o '<title>.*</title>' | sed 's/<\/\?title>//g' | sed 's/ - YouTube//' |
            sed \
                -e "s/&apos;/'/g" \
                -e "s/&#39;/'/g" \
                -e "s/&quot;/\"/g" \
                -e "s/&#34;/\"/g" \
                -e "s/&amp;/\&/g" \
                -e "s/&#38;/\&/g"
    )"

    #gets json of videos
    yt_json="$(get_yt_json "$yt_html")"

    #gets a list of videos
    videos_json="$(printf "%s" "$yt_json" |
        jq '[ .contents | ..|.gridVideoRenderer? |
	select(. !=null) |
	    {
	    	title: .title.runs[0].text,
	    	channel:"'"$channel_name"'",
	    	duration:.thumbnailOverlays[0].thumbnailOverlayTimeStatusRenderer.text.simpleText,
	    	views: .shortViewCountText.simpleText,
	    	date: .publishedTimeText.simpleText,
	    	videoID: .videoId,
	    	thumbs: .thumbnail.thumbnails[0].url,
	    }
	]')"

    videos_json="$(echo "$videos_json" | jq '.[0:'$sub_link_count']')"
    videos_data="$(get_video_data "$videos_json")"

    #if there aren't videos
    [ -z "$videos_data" ] && {
        printf "No results found. Make sure the link is correct.\n"
        exit 1
    }
    if [ "$fancy_subscriptions_menu" -eq 1 ]; then
        printf "             -------%s-------\t\n%s\n" "$channel_name" \
            "$videos_data" >>"$tmp_video_data_file"
    else
        printf "%s\n" "$videos_data" >>"$tmp_video_data_file"
    fi

    [ $show_thumbnails -eq 1 ] && download_thumbnails "$videos_json"
}
scrape_yt() {
    # needs search_query as $*
    ## Scrape data and store video information in videos_data ( and thumbnails )

    #sp is the urlquery youtube uses for sorting videos
    #only runs if --filter-id or --sp was unspecified
    if [ -z "$sp" ]; then
        get_sp_filter
    else
        #youtube puts in %253d one ore more times in the filter id, it doesn't seem useful, so we are removing it if it's in the filter
        sp="${sp%%%*}"
    fi

    yt_html="$(get_yt_html "https://www.youtube.com/results" "$*")"
    if [ -z "$yt_html" ]; then
        print_error "\033[31mERROR[#01]: Couldn't curl website. Please check your network and try again.\033[0m\n"
        exit 1
    fi

    yt_json="$(get_yt_json "$yt_html")"

    #if the data couldn't be found
    if [ -z "$yt_json" ]; then
        print_error "\033[31mERROR[#02]: Couldn't find data on site.\033[0m\n"
        exit 1
    fi

    #gets a list of videos
    videos_json="$(printf "%s" "$yt_json" | jq '[ .contents|
	..|.videoRenderer? |
	select(. !=null) |
		{
			title: .title.runs[0].text,
			channel: .longBylineText.runs[0].text,
			duration:.lengthText.simpleText,
			views: .shortViewCountText.simpleText,
			date: .publishedTimeText.simpleText,
			videoID: .videoId,
			thumbs: .thumbnail.thumbnails[0].url
		}
	]')"

    videos_data="$(get_video_data "$videos_json")"
    #if there aren't videos
    [ -z "$videos_data" ] && {
        printf "No results found. Try different keywords.\n"
        exit 1
    }

    wait
}

############################
#      User selection      #
############################
#> To get search query
get_search_query() {
    #in case no query was provided
    if [ -z "$search_query" ]; then
        #search prompt
        printf "$search_prompt"
        read -r search_query
        [ -z "$search_query" ] && exit 0
    fi
}
#> To select videos from videos_data
user_selection() {
    [ "$is_url" -eq 1 ] && return
    #remove subscription separators
    videos_data_clean="$(printf "%s" "$videos_data" | sed "/.*""$(printf "\t")""$/d")"

    selected_data="$(video_menu "$videos_data")"

    #gets a list of video ids/urls from the selected data
    shorturls="$(echo "$selected_data" | sed -E -n -e "s_.*\|([^|]+) *\$_\1_p")"
    [ -z "$shorturls" ] && exit

    #for each url append the full url to the $urls string
    #through this loop, the selected data which was truncated by formatting is retrived.
    urls=""
    selected_data=""
    while read -r surl; do
        [ -z "$surl" ] && continue
        urls="$(printf '%s\n%s' "${urls}" "https://www.youtube.com/watch?v=$surl")"
        selected_data="$(printf '%s\n%s' "$selected_data" "$(echo "$videos_data" | grep -m1 -e "$surl")")"
    done <<EOF
	$shorturls
EOF
    urls="$(printf "%s" "$urls" | sed 1d)"
    #sometimes % shows up in selected data, could throw an error if it's an invalid directive
    selected_data="$(printf "%s" "$selected_data" | sed 1d)"

    ytv "$urls" -c "new"
}

############################
#         Misc             #
############################
#> if the input is a url then skip video selection and play the url
check_if_url() {
    # to check if given input is a url
    url_regex='^https\?://.*'

    if { echo "$1" | grep -q "$url_regex"; }; then
        is_url=1
        urls="$(echo "$1" | tr ' ' '\n')"
        scrape=""
    else
        is_url=0
    fi
}

is_non_number() {
    if [ -n "$(echo "$1" | grep -o '[^0-9]')" ]; then
        return 0
    else
        return 1
    fi
}

bad_opt_arg() {
    opt="$1"
    arg="$2"
    printf "%s\n" "$opt requires a numeric arg, but was given \"$arg\""
    exit 2
}

#OPT
parse_long_opt() {
    opt="$1"
    #if the option has a short version it calls this function with the opt as the shortopt
    case "${opt}" in
        help) parse_opt "h" ;;
        help-all)
            all_help_info
            exit
            ;;

        is-ext-menu) parse_opt "D" ;;
        is-ext-menu=*)
            parse_opt "D" "${opt#*=}"
            is_non_number "$is_ext_menu" && bad_opt_arg "--ext-menu=" "$is_ext_menu"
            ;;

        download) parse_opt "d" ;;

        choose-from-history) parse_opt "H" ;;

        clear-history) parse_opt "x" ;;

        search-again) parse_opt "s" ;;
        search-again=*)
            parse_opt "s" "${opt#*=}"
            is_non_number "$search_again" && bad_opt_arg "--search=" "$search_again"
            ;;

        loop) parse_opt "l" ;;
        loop=*)
            parse_opt "l" "${opt#*=}"
            is_non_number "$YTFZF_LOOP" && bad_opt_arg "--loop=" "$YTFZF_LOOP"
            ;;

        show-thumbnails) parse_opt "t" ;;
        show-thumbnails=*)
            parse_opt "t" "${opt#*=}"
            is_non_number "$show_thumbnails" && bad_opt_arg "--thumbnails=" "$show_thumbnails"
            ;;

        show-link-only) parse_opt "L" ;;
        show-link-only=*)
            parse_opt "L" "${opt#*=}"
            is_non_number "$show_link_only" && bad_opt_arg "--link-only=" "$show_link_only"
            ;;

        link-count=*) parse_opt "n" "${opt#*=}" ;;

        audio-only) parse_opt "m" ;;

        auto-select) parse_opt "a" ;;
        auto-select=*)
            parse_opt "a" "${opt#*=}"
            is_non_number "$auto_select" && bad_opt_arg "--auto-play=" "$auto_select"
            ;;

        select-all) parse_opt "A" ;;
        select-all=*)
            parse_opt "A" "${opt#*=}"
            is_non_number "$select_all" && bad_opt_arg "--select-all=" "$select_all"
            ;;

        random-select) parse_opt "r" ;;
        random-select=*)
            parse_opt "r" "${opt#*=}"
            is_non_number "$random_select" && bad_opt_arg "--random-play=" "$random_select"
            ;;

        upload-time=*) upload_date_filter="${opt#*=}" ;;
        last-hour) upload_date_filter="last-hour" ;;
        today) upload_date_filter="today" ;;
        this-week) upload_date_filter="this-week" ;;
        this-month) upload_date_filter="this-month" ;;
        this-year) upload_date_filter="this-year" ;;

        upload-sort=*) sort_by_filter="${opt#*=}" ;;
        upload-date) sort_by_filter="upload-date" ;;
        view-count) sort_by_filter="view-count" ;;
        rating) sort_by_filter="rating" ;;

        filter-id=* | sp=*) sp="${opt#*=}" ;;

        preview-side=*) export PREVIEW_SIDE="${opt#*=}" ;;

        update) update_ytfzf "master" ;;
        update-unstable) update_ytfzf "development" ;;

        subs) parse_opt "S" ;;
        subs=*)
            sub_link_count="${opt#*=}"
            is_non_number "$sub_link_count" && bad_opt_arg "--subs" "$sub_link_count"
            parse_opt "S"
            ;;

        fancy-subs) fancy_subscriptions_menu=1 ;;
        fancy-subs=*) fancy_subscriptions_menu="${opt#*=}" ;;

        version)
            printf "\033[1mytfzf:\033[0m %s\n" "$YTFZF_VERSION"
            printf "\033[1myoutube-dl:\033[0m %s\n" "$(youtube-dl --version)"
            command -v "fzf" 1>/dev/null && printf "\033[1mfzf:\033[0m %s\n" "$(fzf --version)"
            exit
            ;;

        *)
            printf "Illegal option --%s\n" "$opt"
            usageinfo
            exit 2
            ;;
    esac
}

#if stdin is given and no input (including -) is given, throw error
#also make sure its not reading from ext_menu
if [ ! -t 0 ] && [ -z "$*" ] && [ $is_ext_menu -eq 0 ]; then
    print_error "\033[31mERROR[#04]: Use - when reading from stdin\033[0m\n"
    exit 2
#read stdin if given
elif [ "$*" = "-" ]; then
    printf "Reading from stdin\n"
    while read -r line; do
        search_query="$search_query $line"
    done
fi
check_if_url "${search_query:=$*}"

#format the menu screen
format_menu

case "$scrape" in
    "yt_search")
        get_search_query
        scrape_yt "$search_query"
        ;;
esac

user_selection
